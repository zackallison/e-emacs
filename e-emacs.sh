#!/bin/env bash

e () {
    # A wrapper function for emacs and emacsclient.  Handles incoming and
    # outgoing pipes, templates

    ### Examples:
    ##
    ## Open $file using emacsclient and starts emacs if there's not already
    ## server running.
    #
    # e [file]

    ##  Normal emacsclient flags gets passed along.  So [c]reate a new frame and
    # [n]o-wait for the client to exit.
    #
    # e -c -n file

    ## Create a new buffer containing the output of a command
    #
    # ls | e

    ## Create a buffer from the output of a command, edit the results, and pass
    ## it on down the pipe.
    #
    # ls -1 *.bak | e | xargs -I{} rm {}

    ## Start with an empty buffer, then pass the buffer on down the pipe.
    #
    # e | rot13  # NOTE: Super Secure encryption

    ## Edit a file, then pass the file on down the pipe.
    #
    # e request.txt | nc www.exampke.com 80

    ## Edit a file as a template, then passing it to the pipe
    #
    # e -t header_template.txt | nc www.example.com 80

    ## Edit multiple files as templates, then passing them to the pipe
    #
    # e -t header_template.html body_template.html footer.html > new_page.html

    ## Keep the new templates (-k implies -t)
    #
    # e -t -k request-template.html | nc www.example.com 80

    ## Store the output of editing the template in ./new/
    ## -o implies -t and -k
    #
    # e -o ./new/ template.html | nc www.example.com 80

    # NOTE: Usage on remote hosts assumes you've set up the remote
    # emacsclient to speak to the local server.
    #
    # Example:
    #
    # Set emacs server to tcp and listening on port 5922 then:
    #
    # function forward_emacs_ssh {
    #     # LC_VARs get passed with the default openssh server settings.
    #     # Assumes the hostname is the last argument
    #     LC_HOSTNAME=${*: -1} LC_EMACS=1 LC_EMACS_SERVER=$(cat ~/.emacs.d/server/server) \
    #                    ssh -R 5922:127.0.0.1:5922  -A "$@"
    # }
    #
    # And on the receiving side in .bashrc or whatever:
    #
    # if [[ "$LC_EMACS_SERVER" ]]; then
    #     ## We're in an SSH session from inside emacs, since openssh allows $LC
    #     ## variables to be passed by default.  (At least on Debian 10.4)
    #     mkdir -p ~/.emacs.d/server 2>&1 > /dev/null
    #     echo "$LC_EMACS_SERVER" > ~/.emacs.d/server/server
    # fi
    #
    # NOTE: The sh forwarding is NOT super secure.  Anyone on localhost could
    # send messages to emacs.  Probably not a good idea on multi-user machines.
    #
    # NOTE: By default it uses the output of the remote hosts `hostname -f`
    # to determine the ssh_hostname.  If this is incorrect then set
    # ssh_hostname using another method, like above.
    #
    # NOTE: The --template / -t option creates temporary copies of the
    # file(s) to use.  They are deleted afterwards unless --keep / -k is
    # given.

    local emacs_opt emacs_tmp emacs_templates emacs_keep emacs_runid emacs_outputdir
    declare -a emacs_tmp_files
    declare -a emacs_allfiles
    ssh_hostname=${ssh_hostname:-$(hostname -f)}
    emacs_outputdir="/tmp"
    # TODO: Add option to specify path to save templates to.

    # Read the command line arguments and separate options from files.
    while [[ $# -gt 0 ]]; do
        local key
        key="$1"
        case $key in
            -t) ;&
            --template) emacs_templates=1; shift;;

            -k) ;&
            --keep) emacs_templates=1; emacs_keep=1; shift;;

            -o) ;&
            --output-dir) emacs_outputdir="$2"; emacs_templates=1; emacs_keep=1; shift ; shift;;

            -*) emacs_opt+="$key "; shift;;

            *) local filename

               filename=$key
               if [[ "$emacs_templates" ]]; then
                   local extension emacs_tmp
                   emacs_runid=$(command -v uuidgen 2>&1 >/dev/null && uuidgen || mktemp XXXXXXXXXXXXXXXX)
                   filename=$(basename -- "$key")
                   extension="${filename##*.}"
                   filename="${filename%%.*}"
                   emacs_tmp="${emacs_outputdir}/${filename}-${emacs_runid}.${extension}"
                   cp "$key" "$emacs_tmp"
                   emacs_allfiles+=("$emacs_tmpfile")
                   emacs_tmp_files+=("$emacs_tmpfile")
               else
                   emacs_allfiles+=("$key");
               fi
               shift;;
        esac
    done

    # If stdin is a pipe write it to a temporary file and open that, then
    # pass the results.  If stdout is a pipe and we and don't have a file
    # then create a temporary file.
    output_pipe_p=0
    if [[ -p /dev/stdin ]]; then
        emacs_tmp="$(mktemp)"
        cat > $emacs_tmpfile
        emacs_allfiles+=("$emacs_tmpfile")
        emacs_tmp_files+=("$emacs_tmpfile")
    elif [[ -z "$emacs_allfiles" && -p /dev/stdout ]]; then
        output_pipe_p=1
        emacs_tmp="$(mktemp)"
        emacs_allfiles+=("$emacs_tmpfile")
        emacs_tmp_files+=("$emacs_tmpfile")
    fi

    # No options given, make our best guess.  If we're inside emacs already
    # just open it.  If we have a GUI running then popup a new frame,
    # otherwise use the current terminal.
    if [[ ! "${emacs_opt}" ]]; then
        if [[ "$INSIDE_EMACS" ]]; then
            emacs_opt=""
        elif [[ "$LC_EMACS_SOCKET" ]]; then
            emacs_opt="-s ${LC_EMACS_SOCKET}"
        fi

        if [[ "$output_pipe_p" == 0 ]]; then
            emacs_opt+=" -n"
        fi
    fi

    ## SSH: make tramp string for remote hosts
    if [[ $SSH_CONNECTION ]]; then
        declare -a _files
        for file in "${emacs_allfiles[@]}"; do
            file="/ssh:${ssh_hostname}:$file"
            _files+=(${file})
        done
        emacs_allfiles=()
        emacs_allfiles+=("${_files[@]}")
    fi

    ## RUN: Execute emacsclient
    echo emacsclient ${emacs_opt} -q "${emacs_allfiles[@]}"
    emacsclient ${emacs_opt} -q "${emacs_allfiles[@]}"

    ## If we're a pipe push everything to stdout
    if [[ -p /dev/stdout ]];then
        cat "${emacs_allfiles[@]}"
    fi

    ## Cleanup:
    ## Remove temporary files, unless told to keep them
    if [[ "${emacs_tmp_files[0]}" && ! "$emacs_keep" ]]; then
        for emacs_tmp in "${emacs_tmp_files[@]}"; do
            # Cleanup tmp files
            rm "$emacs_tmp"
        done
    fi
}
